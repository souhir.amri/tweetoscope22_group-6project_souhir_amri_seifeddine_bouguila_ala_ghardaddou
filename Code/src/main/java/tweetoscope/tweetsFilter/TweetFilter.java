/*
Copyright 2022 Virginie Galtier

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>
 */

 //tweet filter
package tweetoscope.tweetsFilter;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import java.util.Collections;

import java.util.Properties;


import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONException;
import org.json.JSONObject;

import com.twitter.clientlib.model.Tweet;

/**
 * Reacts to the reception of a new Tweet, if the Tweet matches the filter
 * condition, downstream subscribers are notified, otherwise the process is
 * silent. Tweets are received from
 * {@link distributed_tweetoscope.tweetsProducer.TweetsProducer} via Java Flow.
 * Filtered Tweets are passes down to the
 * {@link distributed_tweetoscope.HashtagCounter} via Java Flow.
 * 
 * @author Virginie Galtier
 *
 */
public abstract class TweetFilter {


	protected static KafkaConsumer<Void, String> consumer;
	

	protected static KafkaProducer<Void, String> producer;
	

	protected String bootstrapServers;
	
	
	protected String tweetTopic;
	
	
	protected String filteredTopic;
	public TweetFilter(String bootstrapServers, String tweetTopic, String filteredTopic) {
		this.bootstrapServers = bootstrapServers;
		this.tweetTopic = tweetTopic;
		this.filteredTopic = filteredTopic;
		consumer = new KafkaConsumer<Void, String>(configureKafkaConsumer(bootstrapServers));
		consumer.subscribe(Collections.singletonList(tweetTopic));

		producer = new KafkaProducer<Void, String>(configureKafkaProducer(bootstrapServers));
		
	}
	protected void start() {
		
		try {
			
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				
				//System. out. println(records);
				for (ConsumerRecord<Void, String> record : records) {
					
					
					String message = record.value();
					
					
		    		try {
		    			JSONObject tweetJson = new JSONObject(message);
		    			Tweet tweet = new Tweet();
		    			tweet.setId(tweetJson.getString("author_id"));
		    			tweet.setText(tweetJson.getString("text"));
		    			tweet.setLang(tweetJson.getString("lang"));
		    			try {
			    			OffsetDateTime dt =  OffsetDateTime.parse(tweetJson.getString("created_at"), DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXXXX"));
			    			tweet.setCreatedAt(dt);
		    			} catch (Exception e) {
		    				System.out.println(e.getMessage());
		    			}
			    		if (match(tweet)) {
							producer.send(new ProducerRecord<Void, String>(filteredTopic, null, message));
							
							System.out.println(message);
			    		}
		    		} catch (JSONException e) {
		    			System.out.println("Can't parse JSON object");
		    		}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
			producer.close();
		}
	}
	/**
	 * Prepares configuration for the Kafka producer <Void, String> 
	 * 
	 * @return configuration properties for the Kafka producer
	 */
	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}

	/**
	 * Prepares configuration for the Kafka consumer <Void, String> 
	 * All filters share the "the_filters" consumer group.
	 * 
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_filters");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}

	/**
	 * Tests the filter conditions
	 * 
	 * @param tweet Tweet to examine
	 * @return true if the Tweet complies with the filter, false if it doesn't match
	 *         the filter conditions
	 */
	protected abstract boolean match(Tweet tweet);

}