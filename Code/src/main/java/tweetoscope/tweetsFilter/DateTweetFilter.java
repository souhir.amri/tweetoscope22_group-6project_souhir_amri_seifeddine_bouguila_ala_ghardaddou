package tweetoscope.tweetsFilter;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import com.twitter.clientlib.model.Tweet;
//date filter

public class DateTweetFilter extends TweetFilter {
	/**
	 *Method to match the creation date.
	 */

	protected String creation_date;

	/**
	 * Creates a filter that tests whether the "CreatedAt" tag of a Tweet (if it is
	 * set) equals a given date.
	 * 
	 * @param creation_date target creation_date to match (example: "2022-12-03T19:42:00Z")
	 */
	public DateTweetFilter(String creation_date, String bootstrapServers, String tweetTopicName, String filteredTopicName) {
		super(bootstrapServers, tweetTopicName, filteredTopicName);
		this.creation_date = creation_date;
		
	}

	

	
	
	
	
	
	
	@Override
	protected boolean match(Tweet tweet) {
		OffsetDateTime Date = OffsetDateTime.of(Integer.parseInt(creation_date.substring(0, 4)), Integer.parseInt(creation_date.substring(5, 7)), Integer.parseInt(creation_date.substring(8, 10)), Integer.parseInt(creation_date.substring(11, 13)), Integer.parseInt(creation_date.substring(14, 16)), 0 , 0, ZoneOffset.UTC);
		return tweet.getCreatedAt().isAfter(Date);
	}
}
