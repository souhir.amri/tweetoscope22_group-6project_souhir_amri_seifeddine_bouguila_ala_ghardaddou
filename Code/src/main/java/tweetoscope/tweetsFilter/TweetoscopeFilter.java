package tweetoscope.tweetsFilter;

import tweetoscope.tweetsProducer.TweetoscopeProducer;
//tweetoscope filter app 
public class TweetoscopeFilter {
	
	private static final String FILTER_COUNTRY = "country";
	private static final String FILTER_LANGUAGE = "language";
	private static final String FILTER_DATE = "date";
	private static final String FILTER_NONE = "none";
	private static final String COUNTRY_CODE = "us" ; 
	private static final String CREATION_DATE =  "2016-12-03T19:42:00Z" ;
	
	private static final String LANGUAGE = "en";
	
	protected String bootstrapServers;
	protected String tweetTopic;
	protected String filteredTopic;
	
	
	public static void main(String[] args) {
		new TweetoscopeFilter(args[0],args[1],args[2], args[3]);
		
	}
	
	
	public TweetoscopeFilter(String bootstrapServers,String tweetTopic,String filteredTopic , String filtertype) {
		this.bootstrapServers = bootstrapServers;
		this.tweetTopic = tweetTopic  ;
		this.filteredTopic = filteredTopic;
		
		switch (filtertype) {
		case FILTER_NONE:
			EmptyTweetFilter emptyfilter = new EmptyTweetFilter(bootstrapServers, tweetTopic,  filteredTopic);
			emptyfilter.start();
			break;
		case FILTER_LANGUAGE:
			LangTweetFilter langfilter = new LangTweetFilter(LANGUAGE, bootstrapServers, tweetTopic, filteredTopic);
			langfilter.start();
			break;
		case FILTER_COUNTRY:
			CountryCodeTweetFilter countryfilter = new CountryCodeTweetFilter(COUNTRY_CODE,bootstrapServers, tweetTopic, filteredTopic);
			countryfilter.start();
			break;
		case FILTER_DATE :
			DateTweetFilter datefilter = new DateTweetFilter(CREATION_DATE, bootstrapServers, tweetTopic,  filteredTopic);
			datefilter.start();
			 }
		
	}

}
