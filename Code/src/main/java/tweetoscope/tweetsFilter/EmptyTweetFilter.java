/*
Copyright 2022 Virginie Galtier

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>
 */

 //empty filter
package tweetoscope.tweetsFilter;

import com.twitter.clientlib.model.Tweet;

/**
 * Empty Tweet filter: all Tweets are accepted
 * 
 * @author Virginie Galtier
 *
 */
public class EmptyTweetFilter extends TweetFilter {
	public EmptyTweetFilter(String bootstrapServers, String tweetTopicName, String filteredTopicName) {
		super(bootstrapServers, tweetTopicName, filteredTopicName);
	
	}
	
	/*
	public static void main(String[] args) {
		new EmptyTweetFilter(args[0], args[1], args[2]);
	}
	*/
	
	@Override
	protected boolean match(Tweet tweet) {
		return true;
	}
}
