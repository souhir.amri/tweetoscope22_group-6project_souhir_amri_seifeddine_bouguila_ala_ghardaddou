package tweetoscope;
import java.awt.Color;
import java.awt.Dimension;
import java.time.Duration;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
//visualizor
import java.util.stream.Stream;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.json.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;

@SuppressWarnings("serial")
public class Visualizor extends JFrame {
	
	protected static KafkaConsumer<Void, String> consumer;
    private String bootstrapServers;
    private String HashtagCounterTopicName;
    protected DefaultCategoryDataset dataset;
    protected final static String ROW_KEY = "hashtag";
    protected int nbLeaders;

   
    public Visualizor(String bootstrapServers, String HashtagCounterTopicName , int nbLeader) {
    	
    	
        this.nbLeaders = nbLeader;
        this.bootstrapServers = bootstrapServers;
        this.HashtagCounterTopicName = HashtagCounterTopicName;
        
        
        consumer = new KafkaConsumer<Void, String>(configureKafkaConsumer(bootstrapServers));
        consumer.subscribe(Collections.singletonList(HashtagCounterTopicName));
        dataset = new DefaultCategoryDataset();
        
        
        JFreeChart chart = ChartFactory.createBarChart("Most Popular Hashtags", // title
                "", // category axis label
                "number of occurences", // value axis label
                dataset, // category dataset
                PlotOrientation.HORIZONTAL, // orientation
                false, // legend
                true, // tooltips
                false); // urls
        
        chart.getCategoryPlot().setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
        chart.getCategoryPlot().setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        chartPanel.setBackground(Color.white);
        chartPanel.setPreferredSize(new Dimension(500, 300));
        this.add(chartPanel);
        
        
        this.pack();
        this.setTitle("Tweetoscope");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        
        
        try {
            Duration timeout = Duration.ofMillis(1000);
            while (true) {
               
                ConsumerRecords<Void, String> records = consumer.poll(timeout);
                
                for (ConsumerRecord<Void, String> record : records) {
                   
                    String item = record.value();
                    
                    
                    
                  
               
                    
                    if (item != null) {
                    	
                    	System.out.println(item);
     
						JSONObject json = new JSONObject(item);
						
						dataset.clear();
						Iterator<String> keys = json.keys();
						int i = 0;
						
						while(keys.hasNext()) {
						    String key = keys.next();	
						    i++;
						    
						    dataset.setValue((int) json.get(key), ROW_KEY, key);
							
						}
						
						for (; i < nbLeaders; i++) {
							dataset.setValue(0, ROW_KEY, "");
						}
					}
           
            }
            }
        } catch (Exception e) {
            System.out.println("something went wrong... " + e.getMessage());
        } finally {
            consumer.close();
        }
    }
    
    public static void main(String[] args) {
    	
    	
        new Visualizor(args[0], args[1], Integer.valueOf(args[2]));
    }
    
   
    private Properties configureKafkaConsumer(String bootstrapServers) {
        Properties consumerProperties = new Properties();
        
        consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_visualizors");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); 
        
        return consumerProperties;
    }
}