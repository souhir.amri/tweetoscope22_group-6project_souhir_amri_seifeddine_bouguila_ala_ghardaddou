package tweetoscope.tweetsProducer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import com.twitter.clientlib.model.Tweet;

//recorded

public class MockTwitterStreamRecorded extends OfflineTweetsProducer {

	
	private String TEST_BASE_NAME;
	
	
	
	
	
	
	public MockTwitterStreamRecorded(String TEST_BASE_NAME , String bootstrapServers, String topicName) {
		super(bootstrapServers, topicName);
		
		this.TEST_BASE_NAME = TEST_BASE_NAME;
		
	}
	
	
	public void run() {
		
		try {
			Gson gson = new Gson();
			
			Reader file_reader = new FileReader(TEST_BASE_NAME) ;
			
			JsonObject file_json = gson.fromJson(file_reader, JsonObject.class);
			
			
			JsonArray tweetsarray = file_json.getAsJsonArray("tweets");
			
			
			for (JsonElement tweet_json: tweetsarray) {
				//Tweet tweet = new Tweet();
				JsonObject tweet_object = null;
				try {
				tweet_object =tweet_json.getAsJsonObject();
				} catch ( Exception e) {e.printStackTrace(); continue ;}
				if (tweet_object != null) {
					String tweet_tostring = tweet_object.toString();
					publish(tweet_tostring);
					
				}
				/*tweet.setId(tweet_object.getAsJsonPrimitive("id").getAsString());
				String Date = tweet_object.getAsJsonPrimitive("created_at").getAsString();
				
				tweet.setCreatedAt(OffsetDateTime.of(Integer.parseInt(Date.substring(0, 4)), Integer.parseInt(Date.substring(5, 7)), Integer.parseInt(Date.substring(8, 10)), Integer.parseInt(Date.substring(11, 13)), Integer.parseInt(Date.substring(14, 16)), 0 , 0, ZoneOffset.UTC));

				tweet.setText(tweet_object.getAsJsonPrimitive("text").getAsString());
				tweet.setAuthorId(tweet_object.getAsJsonPrimitive("author_id").getAsString());
				tweet.setConversationId(tweet_object.getAsJsonPrimitive("conversation_id").getAsString());
				tweet.getGeo();
				tweet.setLang(tweet_object.getAsJsonPrimitive("lang").getAsString());
				
				publish(tweet);*/
				
				
				
			}
			
			
		}
		
		catch(FileNotFoundException e) {System.out.println("file not found");System.exit(1);}
		catch (Exception e) {System.out.println("error");}
		finally {
			kafkaProducer.close();
		}
		}
		
		
		
		
	}


