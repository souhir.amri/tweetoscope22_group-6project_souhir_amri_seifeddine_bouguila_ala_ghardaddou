package tweetoscope.tweetsProducer;


import java.util.ArrayList;
import java.util.List;
 //prodcuer


public class TweetoscopeProducer {
	
	
	private static final String SAMPLED_SOURCE = "sampled";
	private static final String FILTERED_SOURCE = "filtered";
	private static final String RANDOM_SOURCE = "random";
	protected static final String SCENARIO_SOURCE = "scenario";
	protected static final String RECORDED_SOURCE = "recorded";
	protected static final String LARGE_TEST_BASE = "largeTestBase.txt";
	protected static final String MINI_TEST_BASE = "miniTestBase.txt";
	protected static final String SCENARIO_TEST_BASE = "scenarioTestBase.txt";
	
	
	private String topicName;
	private String bootstrapServers;
	private String producertype;
	private String TEST_BASE_NAME ;
	
	public static void main(String[] args) {
		new TweetoscopeProducer(args[0],args[1],args[2], args[3]);

	}
	
	public TweetoscopeProducer(String bootstrapServers,String topicName,String producertype , String TEST_BASE_NAME) {
		this.bootstrapServers= bootstrapServers;
        this.topicName= topicName;
        this.producertype = producertype;
        this.TEST_BASE_NAME = TEST_BASE_NAME ;
        switch (producertype) {
        case SAMPLED_SOURCE :
        	TwitterSampledStreamReaderSingleton sampledproducer =  TwitterSampledStreamReaderSingleton.getInstance(bootstrapServers, topicName);
        	sampledproducer.run();
        	break;
        case FILTERED_SOURCE :
        	List<String> keywords = new ArrayList<String>();
			keywords.add("Europe OR Africa OR Asia OR America OR Autralia OR Antartica "
					+ "OR Paris OR Berlin OR London OR Moscow OR Washington OR Beijing "
					+ "OR France OR Germany OR United Kingdom OR Russia OR USA OR China OR Brazil OR Israel "
					+ "OR Atlantic OR Pacific" + "OR place");

			keywords.add("climate OR sustainable OR energy OR free OR open OR peace OR war OR sport "
					+ "OR health OR Internet OR technology OR music OR award OR movie OR star OR pride "
					+ "OR food OR drink OR meal OR recipe OR routine OR economy OR business OR market OR science");

			keywords.add("Machine Learning OR data OR cloud OR social OR network OR woke OR election "
					+ "OR topic OR olympic OR game OR running OR trail OR workout OR challenge OR brand "
					+ "OR marketing OR advertisement OR news OR blockchain OR online OR store OR fashion");

			keywords.add("Monday OR Tuesday OR Wednesday OR Thursday OR Friday OR Saturday OR Sunday " + "OR 2022 "
					+ "OR time " + "OR week OR month OR year " + "OR yesterday OR today OR tomorrow "
					+ "OR last OR next OR soon " + "OR what OR who OR whom OR where OR when OR why");

			keywords.add("Apple OR Google OR Microsoft OR Amazon OR Facebook OR Coca-Cola OR Disney OR Samsung "
					+ "OR Intel OR NIKE OR Cisco 0R Oracle " + "OR Visa OR IBM OR Ikea " + "OR Netflix OR BMW "
					+ "OR Dion OR Thunberg " + "OR Messi OR Federer OR Djokovic " + "OR Rihanna OR Coldplay");

			TwitterFilteredStreamReaderSingleton filteredproducer = TwitterFilteredStreamReaderSingleton.getInstance(keywords , bootstrapServers, topicName);
			filteredproducer.run();
        	break;
        case RANDOM_SOURCE :
        	MockTwitterStreamRandom	randomproducer = new MockTwitterStreamRandom(bootstrapServers, topicName);
        	randomproducer.run();
        	break;
        case SCENARIO_SOURCE :
        	MockTwitterStreamScenario scenarioproducer = new MockTwitterStreamScenario(bootstrapServers, topicName);
        	scenarioproducer.run();
        	break;
        case RECORDED_SOURCE :
        	MockTwitterStreamRecorded recordedproducer = new MockTwitterStreamRecorded( TEST_BASE_NAME ,bootstrapServers , topicName);
        	recordedproducer.run();
        	break;
        }
	}
	
}
