package tweetoscope.tweetsProducer;


 //prodcuer
import com.twitter.clientlib.model.Tweet;
import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;


public abstract class TweetsProducer  {
	
	
	protected KafkaProducer<Void, String> kafkaProducer;
	protected String bootstrapServers;
	protected String topicname;
	
	

	public TweetsProducer(String bootstrapServers, String topicname) {
		this.bootstrapServers = bootstrapServers;
		this.topicname = topicname;
		kafkaProducer = new KafkaProducer<Void, String>(configureKafkaProducer(bootstrapServers));
	}

	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		return producerProperties;
	}
	
	protected void publish(String tweet_string) {
		
		kafkaProducer.send(new ProducerRecord<Void, String>(topicname, null, tweet_string));
		System.out.println(tweet_string);
	}
	
	
}
