

package tweetoscope;
//extractor
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import com.twitter.clientlib.model.Tweet;
import com.twitter.twittertext.Extractor;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONException;
import org.json.JSONObject;



public class HashtagExtractor {
	
	protected static KafkaConsumer<Void, String> consumer;
	protected static KafkaProducer<Void, String> producer;
	protected String bootstrapServers;
	protected String tweetTopicName;
	protected String destinationTopicName;
	final Extractor twitterTextExtractor = new Extractor();

	

	public HashtagExtractor(String bootstrapServers, String tweetTopicName, String destinationTopicName) {
		this.bootstrapServers = bootstrapServers;
		this.tweetTopicName = tweetTopicName;
		this.destinationTopicName = destinationTopicName;
		
		consumer = new KafkaConsumer<Void, String>(configureKafkaConsumer(bootstrapServers));
		
		consumer.subscribe(Collections.singletonList(tweetTopicName));

		producer = new KafkaProducer<Void, String>(configureKafkaProducer(bootstrapServers));
		
		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Void, String> record : records) {
					
					

					String message = record.value();
			        JSONObject tweetjsonObject = new JSONObject(message);    		
		    		Tweet tweet = new Tweet();
		    		try {
		    			tweet.setId(tweetjsonObject.getString("author_id"));
		    			tweet.setText(tweetjsonObject.getString("text"));
		    			tweet.setLang(tweetjsonObject.getString("lang"));
		    			OffsetDateTime dt =  OffsetDateTime.parse(tweetjsonObject.getString("created_at"), DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXXXX"));
		    			tweet.setCreatedAt(dt);
		    		} catch (JSONException e) {
		    			System.out.println("json parsing went wrong... " + e.getMessage());
		    		}

		    		if (tweet != null) {
		    			
		    			List<String> hashtags = twitterTextExtractor.extractHashtags(tweet.getText());

		    			
		    			for (String hashtag : hashtags) {
							producer.send(new ProducerRecord<Void, String>(destinationTopicName, null, hashtag));
							System.out.println(hashtag);
		    			}
		    		}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
			producer.close();
		}
	}

	
	
	public static void main(String[] args) {
		new HashtagExtractor(args[0], args[1], args[2]);
	}
	
	
	
	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,org.apache.kafka.common.serialization.StringSerializer.class.getName());
		return producerProperties;
	}

	
	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_extractor");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		return consumerProperties;
	}
	
}

