package tweetoscope;

import java.time.Duration;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.json.JSONObject;

 //counter


public class HashtagCounter{
	
	protected static KafkaConsumer<Void, String> consumer;
	protected static KafkaProducer<Void, String> producer;
	protected String bootstrapServers;
	protected String tweetTopicName;
	protected String destinationTopicName;
	protected int nbLeaders;
	protected Map<String, Integer> hashtagOccurrenceMap;
	protected Map<String, Integer> previousLeaderMap;

	
	public HashtagCounter(String bootstrapServers, String tweetTopicName, String destinationTopicName, int nbLeader) {
		
		this.nbLeaders = nbLeader;
		
		hashtagOccurrenceMap = new HashMap<String, Integer>();

		this.bootstrapServers = bootstrapServers;
		this.tweetTopicName = tweetTopicName;
		this.destinationTopicName = destinationTopicName;
		
		consumer = new KafkaConsumer<Void, String>(configureKafkaConsumer(bootstrapServers));
		
		consumer.subscribe(Collections.singletonList(tweetTopicName));

		producer = new KafkaProducer<Void, String>(configureKafkaProducer(bootstrapServers));
		
		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Void, String> record : records) {
					
					String hashtag = record.value();

		    		if (hashtag != null) {
		    			synchronized (hashtagOccurrenceMap) {
		    				
		    				String key = "#" + hashtag;
		    				if (hashtagOccurrenceMap.containsKey(key)) {
		    					hashtagOccurrenceMap.replace(key, 1 + hashtagOccurrenceMap.get(key));
		    				} else {
		    					hashtagOccurrenceMap.put(key, 1);
		    				}

		    				
		    				Map<String, Integer> topHashtagsMap = hashtagOccurrenceMap.entrySet().stream()
		    						.sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(nbLeaders)
		    						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		    				
		    				if (previousLeaderMap == null || !previousLeaderMap.equals(topHashtagsMap)) {
		    					displayTop(topHashtagsMap);
		    					previousLeaderMap = topHashtagsMap;
		    				}
		    				
		    			}
		    		}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
			producer.close();
		}
		
	}
	
	public static void main(String[] args) {
		
		new HashtagCounter(args[0], args[1], args[2], Integer.valueOf(args[3]));
	}
	
	private void displayTop(Map<String, Integer> hashtags) {
		
		Iterator<Entry<String, Integer>> hashtagsIterator = hashtags.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).iterator();
		int j = 0;
		Entry<String, Integer> entry = null;
		String message;
		JSONObject json = new JSONObject();


		while ((hashtagsIterator.hasNext()) && (j < this.nbLeaders)) {
			entry = hashtagsIterator.next();
			json.put(entry.getKey(), entry.getValue());
			j++;
			
		}
		
		
		message = json.toString();
		
		producer.send(new ProducerRecord<Void, String>(destinationTopicName, null, message));
		System.out.println(message);
	}
	
	
	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}

	
	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); 

		return consumerProperties;
	}
}